package com.ccizer.configurationreaderapp.configurationreaderapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConfigurationreaderappApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConfigurationreaderappApplication.class, args);
	}
}
